import React, { Component } from "react";
import ReactHLS from 'react-hls';

class Home extends Component{
    render(){
        return(
            <div className="Home">
                <div className="lander">
                    <h1>Strife Solutions</h1>
                    <p>Welcome to the greatest WEB APP HOME PAGE EVER!!!!</p>
                </div>
                <div className="Player">
                    <ReactHLS url={"https://content.jwplatform.com/manifests/yp34SRmf.m3u8"} />
                </div>
            </div>
        );
    }
}

export default Home;