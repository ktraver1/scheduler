import React, { Component } from "react";

class Footer extends Component{
    render(){
        return(
            <div className="Footer">
                <div className="lander">
                    <h1>Footer</h1>
                    <p>Developer: Kevin Travers</p>
                    <p>Company: CBS</p>
                    <p>Contact: kevin.travers@cbs.com</p>
                </div>
            </div>
        );
    }
}

export default Footer;