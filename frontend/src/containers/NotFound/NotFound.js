import React, { Component } from "react";

class NotFound extends Component{
    render(){
        return(
            <div className="NotFound">
                <div className="lander">
                    <h1>Page not found</h1>
                </div>
            </div>
        );
    }
}
export default NotFound;