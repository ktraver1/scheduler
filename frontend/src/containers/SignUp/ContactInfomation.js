import React from "react";
import useSignUpForm from "../../hooks/SubmitFormHook"

const ContactInfomation = () => {
   const nextPage = () => {
      alert(`User Created!
             Name: ${inputs.firstName} ${inputs.lastName}
             Company Name: ${inputs.companyName}`);
    }
  const {inputs, handleInputChange, handleSubmit} = useSignUpForm(nextPage);
   return(
      <div>
            ADD POP UPS OVER EACH TEXT FEILD to say what it is
            SHOW PROGRESS BAR ON TOP
            <h1>Contact Infomation</h1>
            <form onSubmit={handleSubmit}>
               <div>
                  <label>First Name</label>
                  <input type="text" name="firstName" 
                  onChange={handleInputChange} 
                  required />
                  <label>Last Name</label>
                  <input type="text" name="lastName" 
                  onChange={handleInputChange} 
                  required />
               </div>
                <div>
                    <label>Company name</label>
                    <input type="text" name="companyName" 
                    onChange={handleInputChange} 
                    required />
                </div>
                <div>
                    <label>Phone number</label>
                    <input type="tel" name="phoneNumber" 
                    onChange={handleInputChange} 
                    required />
                </div>
                <div>
                    <label>Country/Region</label>
                    <input type="text" name="country" 
                    onChange={handleInputChange} 
                    required />
                </div>
                <div>
                    <label>Address</label>
                    <input type="text" name="address" 
                    onChange={handleInputChange} 
                    required />
                </div>
                <div>
                    <label>Apartment, suite, unit, building, floor, etc.</label>
                    <input type="text" name="apartment" 
                    onChange={handleInputChange} 
                     />
                </div>
                <div>
                    <label>City</label>
                    <input type="text" name="city" 
                    onChange={handleInputChange} 
                    required />
                </div>
                <div>
                    <label>State</label>
                    <input type="text" name="state" 
                    onChange={handleInputChange} 
                    required />
                </div>
                <div>
                    <label>Postal</label>
                    <input type="text" name="postal" 
                    onChange={handleInputChange} 
                    required />
                </div>
                <button type="submit">Continue</button>
            </form>
            <p>Check here to indicate that you have read and agree to the terms of the AWS Customer Agreement</p>

        </div>
   );
   
}

export default ContactInfomation;