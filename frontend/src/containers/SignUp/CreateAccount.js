import React from "react";
import useSignUpForm from "../../hooks/SubmitFormHook"
 
const CreateAccount = () =>{
    const nextPage = () => {
        alert(`User Created!
               Email: ${inputs.email}
               Account Name: ${inputs.accountName}`);
      }
    const {inputs, handleInputChange, handleSubmit} = useSignUpForm(nextPage);
    return(
        <div>
            ADD POP UPS OVER EACH TEXT FEILD to say what it is
            SHOW PROGRESS BAR ON TOP
            <h1>Create a Strife Sheduler Account</h1>
            <form onSubmit={handleSubmit}>
                <div>
                    <label>Account Name</label>
                    <input type="text" name="accountName" 
                    onChange={handleInputChange} 
                    required />
                </div>
                <div>
                    <label>Email Address</label>
                    <input type="email" name="email" 
                    onChange={handleInputChange} 
                    required />
                </div>
                <div>
                    <label>Password</label>
                    <input type="password" name="password1" 
                    onChange={handleInputChange} 
                    required />
                </div>
                <div>
                    <label>Re-enter Password</label>
                    <input type="password" name="password2" 
                    onChange={handleInputChange} 
                    required />
                </div>
                <button type="submit">Continue</button>
            </form>
        </div>
    );
}

export default CreateAccount;