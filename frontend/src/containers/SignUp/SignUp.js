import React from "react";
import useSignUpForm from "../../hooks/SubmitFormHook"



const SignUp = () => {
    const nextPage = () => {
        alert(`User Created!
               Email: ${inputs.email}
               Account Name: ${inputs.accountName}
               Name: ${inputs.firstName} ${inputs.lastName}
               Company Name: ${inputs.companyName}`);
      }
    const {inputs, handleInputChange, handleSubmit} = useSignUpForm(nextPage);

    
    return (
<div>
            ADD POP UPS OVER EACH TEXT FEILD to say what it is
            <h1>Create a Strife Account</h1>
            <form onSubmit={handleSubmit}>
                <div>
                    <label>Account Name</label>
                    <input type="text" name="accountName" 
                    onChange={handleInputChange} 
                    required />
                </div>
                <div>
                    <label>Email Address</label>
                    <input type="email" name="email" 
                    onChange={handleInputChange} 
                    required />
                </div>
                <div>
                    <label>Password</label>
                    <input type="password" name="password1" 
                    onChange={handleInputChange} 
                    required />
                </div>
                <div>
                    <label>Re-enter Password</label>
                    <input type="password" name="password2" 
                    onChange={handleInputChange} 
                    required />
                </div>
                
                <h1>Contact Infomation</h1>
                <div>
                  <label>First Name</label>
                  <input type="text" name="firstName" 
                  onChange={handleInputChange} 
                  required />
                  <label>Last Name</label>
                  <input type="text" name="lastName" 
                  onChange={handleInputChange} 
                  required />
               </div>
                <div>
                    <label>Company name</label>
                    <input type="text" name="companyName" 
                    onChange={handleInputChange} 
                    required />
                </div>
                <div>
                    <label>Phone number</label>
                    <input type="tel" name="phoneNumber" 
                    onChange={handleInputChange} 
                    required />
                </div>
                <div>
                    <label>Country/Region</label>
                    <input type="text" name="country" 
                    onChange={handleInputChange} 
                    required />
                </div>
                <div>
                    <label>Address</label>
                    <input type="text" name="address" 
                    onChange={handleInputChange} 
                    required />
                </div>
                <div>
                    <label>Apartment, suite, unit, building, floor, etc.</label>
                    <input type="text" name="apartment" 
                    onChange={handleInputChange} 
                     />
                </div>
                <div>
                    <label>City</label>
                    <input type="text" name="city" 
                    onChange={handleInputChange} 
                    required />
                </div>
                <div>
                    <label>State</label>
                    <input type="text" name="state" 
                    onChange={handleInputChange} 
                    required />
                </div>
                <div>
                    <label>Postal</label>
                    <input type="text" name="postal" 
                    onChange={handleInputChange} 
                    required />
                </div>
                <button type="submit">Submit</button>
            </form>
        </div>
    );
}

export default SignUp;
