import React, { Component } from "react";
import { Navbar, Nav} from "react-bootstrap";
import { LinkContainer } from 'react-router-bootstrap'
import logo from '../../assets/logo.png';
import "./MyNavbar.css"

class MyNavbar extends Component{
    render(){
        return(
            <div className="Navbar">
                <Navbar bg="dark"  variant="dark" expand="lg" >
                <Navbar.Brand >
                    <LinkContainer to="/home">
                        <Nav.Link >                                
                            <img
                            src={logo}
                            width="30"
                            height="30"
                            className="d-inline-block align-top"
                            alt="Strife Scheduler"
                            />
                        </Nav.Link>
                    </LinkContainer>
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="mr-auto">

                </Nav>
                <Nav  className="nav-item">
                    <LinkContainer to="/home">
                        <Nav.Link >Home</Nav.Link>
                    </LinkContainer>
                    <LinkContainer to="/sign_up">
                        <Nav.Link >Create Account</Nav.Link>
                    </LinkContainer>
                    <LinkContainer  to="/login">
                        <Nav.Link >Login</Nav.Link>
                    </LinkContainer>
                </Nav>
                </Navbar.Collapse>
                </Navbar>
            </div>
        );
    }
}

export default MyNavbar;