import React, { Component } from "react";
import "./App.css";
import Routes from "./Routes";
import MyNavbar from "./containers/MyNavbar/MyNavbar";
import Footer from "./containers/Footer/Footer";

// App will be the class that contains all features all pages have
// App will contain the navbar and footer so all other pages also have them as well

class App extends Component{
  render() {
    return (
      <div className="App container">
        <MyNavbar />
        <Routes />
        <Footer />
      </div>
    );
  }
  

}

export default App;
