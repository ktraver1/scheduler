import React from "react";
import { Route, Switch } from "react-router-dom";
import Home from "./containers/Home/Home";
import Login from "./containers/Login/Login";
import SignUp from "./containers/SignUp/SignUp";
import NotFound from "./containers/NotFound/NotFound";

//TODO LOOK INTO ROUTER
export default () =>
  <Switch>
    { /* need 2 paths home for navbar to work*/ }
    <Route path="/" exact component={Home} />
    <Route path="/home" exact component={Home} />
    <Route path="/login/" exact component={Login} />
    <Route path="/sign_up/" exact component={SignUp} />
    { /* Finally, catch all unmatched routes */ }
    <Route component={NotFound} />
  </Switch>;
